﻿
namespace Szakdolgozat_OBYBZK
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.iconButtonWordProcessor = new FontAwesome.Sharp.IconButton();
            this.iconButtonDocx = new FontAwesome.Sharp.IconButton();
            this.iconButtonPdf = new FontAwesome.Sharp.IconButton();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelMainSpace = new System.Windows.Forms.Panel();
            this.richTextBoxLoadDocx = new System.Windows.Forms.RichTextBox();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.axAcroPDF = new AxAcroPDFLib.AxAcroPDF();
            this.buttonDo = new System.Windows.Forms.Button();
            this.checkBoxMetaXml = new System.Windows.Forms.CheckBox();
            this.checkBoxMetaTxt = new System.Windows.Forms.CheckBox();
            this.checkBoxGetMetadata = new System.Windows.Forms.CheckBox();
            this.checkBoxDocxToTxt = new System.Windows.Forms.CheckBox();
            this.checkBoxGetTable = new System.Windows.Forms.CheckBox();
            this.checkBoxGetPictures = new System.Windows.Forms.CheckBox();
            this.checkBoxGetPicturesFromDocx = new System.Windows.Forms.CheckBox();
            this.checkBoxPdfToTxt = new System.Windows.Forms.CheckBox();
            this.checkBoxDocxToPdf = new System.Windows.Forms.CheckBox();
            this.checkBoxPdfToDocx = new System.Windows.Forms.CheckBox();
            this.buttonPreview_2 = new System.Windows.Forms.PictureBox();
            this.buttonSearchFile = new System.Windows.Forms.Button();
            this.textBoxRoad = new RoundTextBox();
            this.labelRoad = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.fontToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.colorToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.leftToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.centerToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.rightToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.boldToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.italicToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.underlineToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDocxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.saveAsPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsDocxFájlkéntToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.insertImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectFontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fontColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.boldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.italicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.pageColorHáttérszínToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paragraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.behúzásToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indent0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indent5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indent10ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.indent15ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.indent20ToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.igazításToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSpace = new System.Windows.Forms.Panel();
            this.panelCurrentMenu = new System.Windows.Forms.Panel();
            this.labelWhichMenu = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panelSideMenu.SuspendLayout();
            this.panelMainSpace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPreview_2)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelCurrentMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(30)))), ((int)(((byte)(68)))));
            this.panelSideMenu.Controls.Add(this.iconButtonWordProcessor);
            this.panelSideMenu.Controls.Add(this.iconButtonDocx);
            this.panelSideMenu.Controls.Add(this.iconButtonPdf);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(259, 760);
            this.panelSideMenu.TabIndex = 0;
            // 
            // iconButtonWordProcessor
            // 
            this.iconButtonWordProcessor.BackgroundImage = global::Szakdolgozat_OBYBZK.Properties.Resources.wordprocessor_menu;
            this.iconButtonWordProcessor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.iconButtonWordProcessor.FlatAppearance.BorderSize = 0;
            this.iconButtonWordProcessor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButtonWordProcessor.IconChar = FontAwesome.Sharp.IconChar.None;
            this.iconButtonWordProcessor.IconColor = System.Drawing.Color.Black;
            this.iconButtonWordProcessor.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButtonWordProcessor.Location = new System.Drawing.Point(0, 536);
            this.iconButtonWordProcessor.Name = "iconButtonWordProcessor";
            this.iconButtonWordProcessor.Size = new System.Drawing.Size(259, 208);
            this.iconButtonWordProcessor.TabIndex = 3;
            this.iconButtonWordProcessor.Text = "\r\n";
            this.iconButtonWordProcessor.UseVisualStyleBackColor = true;
            this.iconButtonWordProcessor.Click += new System.EventHandler(this.iconButtonWordProcessor_Click);
            // 
            // iconButtonDocx
            // 
            this.iconButtonDocx.BackgroundImage = global::Szakdolgozat_OBYBZK.Properties.Resources.wordpng_menu_3;
            this.iconButtonDocx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.iconButtonDocx.FlatAppearance.BorderSize = 0;
            this.iconButtonDocx.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButtonDocx.IconChar = FontAwesome.Sharp.IconChar.None;
            this.iconButtonDocx.IconColor = System.Drawing.Color.Black;
            this.iconButtonDocx.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButtonDocx.Location = new System.Drawing.Point(0, 323);
            this.iconButtonDocx.Name = "iconButtonDocx";
            this.iconButtonDocx.Size = new System.Drawing.Size(259, 208);
            this.iconButtonDocx.TabIndex = 2;
            this.iconButtonDocx.Text = "\r\n";
            this.iconButtonDocx.UseVisualStyleBackColor = true;
            this.iconButtonDocx.Click += new System.EventHandler(this.iconButtonDocx_Click);
            // 
            // iconButtonPdf
            // 
            this.iconButtonPdf.BackgroundImage = global::Szakdolgozat_OBYBZK.Properties.Resources.pdfpng_menu;
            this.iconButtonPdf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.iconButtonPdf.FlatAppearance.BorderSize = 0;
            this.iconButtonPdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButtonPdf.IconChar = FontAwesome.Sharp.IconChar.None;
            this.iconButtonPdf.IconColor = System.Drawing.Color.Black;
            this.iconButtonPdf.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButtonPdf.Location = new System.Drawing.Point(0, 110);
            this.iconButtonPdf.Name = "iconButtonPdf";
            this.iconButtonPdf.Size = new System.Drawing.Size(259, 208);
            this.iconButtonPdf.TabIndex = 1;
            this.iconButtonPdf.Text = "\r\n";
            this.iconButtonPdf.UseVisualStyleBackColor = true;
            this.iconButtonPdf.Click += new System.EventHandler(this.iconButtonPdf_Click_1);
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.Transparent;
            this.panelLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelLogo.BackgroundImage")));
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(259, 104);
            this.panelLogo.TabIndex = 0;
            this.panelLogo.Click += new System.EventHandler(this.panelLogo_Click);
            // 
            // panelMainSpace
            // 
            this.panelMainSpace.Controls.Add(this.richTextBoxLoadDocx);
            this.panelMainSpace.Controls.Add(this.labelWelcome);
            this.panelMainSpace.Controls.Add(this.axAcroPDF);
            this.panelMainSpace.Controls.Add(this.buttonDo);
            this.panelMainSpace.Controls.Add(this.checkBoxMetaXml);
            this.panelMainSpace.Controls.Add(this.checkBoxMetaTxt);
            this.panelMainSpace.Controls.Add(this.checkBoxGetMetadata);
            this.panelMainSpace.Controls.Add(this.checkBoxDocxToTxt);
            this.panelMainSpace.Controls.Add(this.checkBoxGetTable);
            this.panelMainSpace.Controls.Add(this.checkBoxGetPictures);
            this.panelMainSpace.Controls.Add(this.checkBoxGetPicturesFromDocx);
            this.panelMainSpace.Controls.Add(this.checkBoxPdfToTxt);
            this.panelMainSpace.Controls.Add(this.checkBoxDocxToPdf);
            this.panelMainSpace.Controls.Add(this.checkBoxPdfToDocx);
            this.panelMainSpace.Controls.Add(this.buttonPreview_2);
            this.panelMainSpace.Controls.Add(this.buttonSearchFile);
            this.panelMainSpace.Controls.Add(this.textBoxRoad);
            this.panelMainSpace.Controls.Add(this.labelRoad);
            this.panelMainSpace.Controls.Add(this.richTextBox1);
            this.panelMainSpace.Controls.Add(this.toolStrip1);
            this.panelMainSpace.Controls.Add(this.menuStrip1);
            this.panelMainSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainSpace.ForeColor = System.Drawing.Color.White;
            this.panelMainSpace.Location = new System.Drawing.Point(259, 110);
            this.panelMainSpace.Name = "panelMainSpace";
            this.panelMainSpace.Size = new System.Drawing.Size(1281, 650);
            this.panelMainSpace.TabIndex = 3;
            // 
            // richTextBoxLoadDocx
            // 
            this.richTextBoxLoadDocx.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBoxLoadDocx.Location = new System.Drawing.Point(707, 6);
            this.richTextBoxLoadDocx.Name = "richTextBoxLoadDocx";
            this.richTextBoxLoadDocx.Size = new System.Drawing.Size(551, 632);
            this.richTextBoxLoadDocx.TabIndex = 46;
            this.richTextBoxLoadDocx.Text = "";
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Century Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWelcome.Location = new System.Drawing.Point(326, 250);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(709, 42);
            this.labelWelcome.TabIndex = 45;
            this.labelWelcome.Text = "A kezdéshez válasszon egy menüpontot!";
            // 
            // axAcroPDF
            // 
            this.axAcroPDF.Enabled = true;
            this.axAcroPDF.Location = new System.Drawing.Point(707, 6);
            this.axAcroPDF.Name = "axAcroPDF";
            this.axAcroPDF.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF.OcxState")));
            this.axAcroPDF.Size = new System.Drawing.Size(551, 632);
            this.axAcroPDF.TabIndex = 44;
            // 
            // buttonDo
            // 
            this.buttonDo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDo.ForeColor = System.Drawing.Color.Black;
            this.buttonDo.Location = new System.Drawing.Point(502, 581);
            this.buttonDo.Name = "buttonDo";
            this.buttonDo.Size = new System.Drawing.Size(132, 30);
            this.buttonDo.TabIndex = 43;
            this.buttonDo.Text = "Végrehajtás";
            this.buttonDo.UseVisualStyleBackColor = true;
            this.buttonDo.Click += new System.EventHandler(this.buttonDo_Click);
            // 
            // checkBoxMetaXml
            // 
            this.checkBoxMetaXml.AutoSize = true;
            this.checkBoxMetaXml.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxMetaXml.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxMetaXml.ForeColor = System.Drawing.Color.White;
            this.checkBoxMetaXml.Location = new System.Drawing.Point(303, 583);
            this.checkBoxMetaXml.Name = "checkBoxMetaXml";
            this.checkBoxMetaXml.Size = new System.Drawing.Size(71, 28);
            this.checkBoxMetaXml.TabIndex = 42;
            this.checkBoxMetaXml.Text = "XML";
            this.checkBoxMetaXml.UseVisualStyleBackColor = false;
            // 
            // checkBoxMetaTxt
            // 
            this.checkBoxMetaTxt.AutoSize = true;
            this.checkBoxMetaTxt.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxMetaTxt.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxMetaTxt.ForeColor = System.Drawing.Color.White;
            this.checkBoxMetaTxt.Location = new System.Drawing.Point(199, 583);
            this.checkBoxMetaTxt.Name = "checkBoxMetaTxt";
            this.checkBoxMetaTxt.Size = new System.Drawing.Size(55, 28);
            this.checkBoxMetaTxt.TabIndex = 41;
            this.checkBoxMetaTxt.Text = "TxT";
            this.checkBoxMetaTxt.UseVisualStyleBackColor = false;
            // 
            // checkBoxGetMetadata
            // 
            this.checkBoxGetMetadata.AutoSize = true;
            this.checkBoxGetMetadata.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxGetMetadata.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxGetMetadata.ForeColor = System.Drawing.Color.White;
            this.checkBoxGetMetadata.Location = new System.Drawing.Point(116, 532);
            this.checkBoxGetMetadata.Name = "checkBoxGetMetadata";
            this.checkBoxGetMetadata.Size = new System.Drawing.Size(365, 28);
            this.checkBoxGetMetadata.TabIndex = 40;
            this.checkBoxGetMetadata.Text = "PDF fájl metaadatainak mentése";
            this.checkBoxGetMetadata.UseVisualStyleBackColor = false;
            this.checkBoxGetMetadata.CheckedChanged += new System.EventHandler(this.checkBoxGetMetadata_CheckedChanged_1);
            // 
            // checkBoxDocxToTxt
            // 
            this.checkBoxDocxToTxt.AutoSize = true;
            this.checkBoxDocxToTxt.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxDocxToTxt.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxDocxToTxt.ForeColor = System.Drawing.Color.White;
            this.checkBoxDocxToTxt.Location = new System.Drawing.Point(116, 485);
            this.checkBoxDocxToTxt.Name = "checkBoxDocxToTxt";
            this.checkBoxDocxToTxt.Size = new System.Drawing.Size(433, 28);
            this.checkBoxDocxToTxt.TabIndex = 39;
            this.checkBoxDocxToTxt.Text = "Docx fájl TxT formátummá konvertálása";
            this.checkBoxDocxToTxt.UseVisualStyleBackColor = false;
            // 
            // checkBoxGetTable
            // 
            this.checkBoxGetTable.AutoSize = true;
            this.checkBoxGetTable.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxGetTable.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxGetTable.ForeColor = System.Drawing.Color.White;
            this.checkBoxGetTable.Location = new System.Drawing.Point(116, 352);
            this.checkBoxGetTable.Name = "checkBoxGetTable";
            this.checkBoxGetTable.Size = new System.Drawing.Size(407, 28);
            this.checkBoxGetTable.TabIndex = 38;
            this.checkBoxGetTable.Text = "PDF fájlban lévő táblázatok mentése";
            this.checkBoxGetTable.UseVisualStyleBackColor = false;
            // 
            // checkBoxGetPictures
            // 
            this.checkBoxGetPictures.AutoSize = true;
            this.checkBoxGetPictures.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxGetPictures.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxGetPictures.ForeColor = System.Drawing.Color.White;
            this.checkBoxGetPictures.Location = new System.Drawing.Point(116, 442);
            this.checkBoxGetPictures.Name = "checkBoxGetPictures";
            this.checkBoxGetPictures.Size = new System.Drawing.Size(299, 28);
            this.checkBoxGetPictures.TabIndex = 37;
            this.checkBoxGetPictures.Text = "PDF fájl képeinek mentése";
            this.checkBoxGetPictures.UseVisualStyleBackColor = false;
            // 
            // checkBoxGetPicturesFromDocx
            // 
            this.checkBoxGetPicturesFromDocx.AutoSize = true;
            this.checkBoxGetPicturesFromDocx.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxGetPicturesFromDocx.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxGetPicturesFromDocx.Location = new System.Drawing.Point(116, 355);
            this.checkBoxGetPicturesFromDocx.Name = "checkBoxGetPicturesFromDocx";
            this.checkBoxGetPicturesFromDocx.Size = new System.Drawing.Size(319, 28);
            this.checkBoxGetPicturesFromDocx.TabIndex = 36;
            this.checkBoxGetPicturesFromDocx.Text = "Képek mentése Docx fájlból";
            this.checkBoxGetPicturesFromDocx.UseVisualStyleBackColor = false;
            // 
            // checkBoxPdfToTxt
            // 
            this.checkBoxPdfToTxt.AutoSize = true;
            this.checkBoxPdfToTxt.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxPdfToTxt.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxPdfToTxt.ForeColor = System.Drawing.Color.White;
            this.checkBoxPdfToTxt.Location = new System.Drawing.Point(116, 262);
            this.checkBoxPdfToTxt.Name = "checkBoxPdfToTxt";
            this.checkBoxPdfToTxt.Size = new System.Drawing.Size(273, 28);
            this.checkBoxPdfToTxt.TabIndex = 35;
            this.checkBoxPdfToTxt.Text = "PDF fájl TxT konvertálása";
            this.checkBoxPdfToTxt.UseVisualStyleBackColor = false;
            // 
            // checkBoxDocxToPdf
            // 
            this.checkBoxDocxToPdf.AutoSize = true;
            this.checkBoxDocxToPdf.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxDocxToPdf.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxDocxToPdf.ForeColor = System.Drawing.Color.White;
            this.checkBoxDocxToPdf.Location = new System.Drawing.Point(116, 219);
            this.checkBoxDocxToPdf.Name = "checkBoxDocxToPdf";
            this.checkBoxDocxToPdf.Size = new System.Drawing.Size(301, 28);
            this.checkBoxDocxToPdf.TabIndex = 34;
            this.checkBoxDocxToPdf.Text = "Docx fájl PDF konvertálása";
            this.checkBoxDocxToPdf.UseVisualStyleBackColor = false;
            // 
            // checkBoxPdfToDocx
            // 
            this.checkBoxPdfToDocx.AutoSize = true;
            this.checkBoxPdfToDocx.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxPdfToDocx.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxPdfToDocx.ForeColor = System.Drawing.Color.White;
            this.checkBoxPdfToDocx.Location = new System.Drawing.Point(116, 172);
            this.checkBoxPdfToDocx.Name = "checkBoxPdfToDocx";
            this.checkBoxPdfToDocx.Size = new System.Drawing.Size(301, 28);
            this.checkBoxPdfToDocx.TabIndex = 26;
            this.checkBoxPdfToDocx.Text = "PDF fájl Docx konvertálása";
            this.checkBoxPdfToDocx.UseVisualStyleBackColor = false;
            // 
            // buttonPreview_2
            // 
            this.buttonPreview_2.BackColor = System.Drawing.Color.Transparent;
            this.buttonPreview_2.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.eye_3_48;
            this.buttonPreview_2.InitialImage = ((System.Drawing.Image)(resources.GetObject("buttonPreview_2.InitialImage")));
            this.buttonPreview_2.Location = new System.Drawing.Point(584, 52);
            this.buttonPreview_2.Name = "buttonPreview_2";
            this.buttonPreview_2.Size = new System.Drawing.Size(50, 44);
            this.buttonPreview_2.TabIndex = 24;
            this.buttonPreview_2.TabStop = false;
            this.buttonPreview_2.Click += new System.EventHandler(this.buttonPreview_2_Click);
            // 
            // buttonSearchFile
            // 
            this.buttonSearchFile.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSearchFile.ForeColor = System.Drawing.Color.Black;
            this.buttonSearchFile.Location = new System.Drawing.Point(283, 105);
            this.buttonSearchFile.Name = "buttonSearchFile";
            this.buttonSearchFile.Size = new System.Drawing.Size(132, 30);
            this.buttonSearchFile.TabIndex = 21;
            this.buttonSearchFile.Text = "Tallózás";
            this.buttonSearchFile.UseVisualStyleBackColor = true;
            this.buttonSearchFile.Click += new System.EventHandler(this.buttonSearchFile_Click);
            // 
            // textBoxRoad
            // 
            this.textBoxRoad.Location = new System.Drawing.Point(104, 67);
            this.textBoxRoad.Name = "textBoxRoad";
            this.textBoxRoad.Size = new System.Drawing.Size(445, 20);
            this.textBoxRoad.TabIndex = 20;
            // 
            // labelRoad
            // 
            this.labelRoad.AutoSize = true;
            this.labelRoad.BackColor = System.Drawing.Color.Transparent;
            this.labelRoad.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRoad.ForeColor = System.Drawing.Color.White;
            this.labelRoad.Location = new System.Drawing.Point(100, 25);
            this.labelRoad.Name = "labelRoad";
            this.labelRoad.Size = new System.Drawing.Size(102, 24);
            this.labelRoad.TabIndex = 7;
            this.labelRoad.Text = "Elérési út:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox1.Location = new System.Drawing.Point(17, 67);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1252, 571);
            this.richTextBox1.TabIndex = 6;
            this.richTextBox1.Text = "";
            this.richTextBox1.SelectionChanged += new System.EventHandler(this.richTextBox1_SelectionChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator5,
            this.fontToolStripButton,
            this.colorToolStripButton,
            this.toolStripSeparator6,
            this.leftToolStripButton,
            this.centerToolStripButton,
            this.rightToolStripButton,
            this.toolStripSeparator7,
            this.boldToolStripButton,
            this.italicToolStripButton,
            this.underlineToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1281, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.new_file;
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "Új";
            this.newToolStripButton.Click += new System.EventHandler(this.newToolStripButton_Click);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.open_file;
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Megnyitás";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.save_file;
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Mentés";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // fontToolStripButton
            // 
            this.fontToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fontToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.font;
            this.fontToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fontToolStripButton.Name = "fontToolStripButton";
            this.fontToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.fontToolStripButton.Text = "Betűtípus";
            this.fontToolStripButton.Click += new System.EventHandler(this.fontToolStripButton_Click);
            // 
            // colorToolStripButton
            // 
            this.colorToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.colorToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.font_color;
            this.colorToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colorToolStripButton.Name = "colorToolStripButton";
            this.colorToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.colorToolStripButton.Text = "Betűszín";
            this.colorToolStripButton.Click += new System.EventHandler(this.colorToolStripButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // leftToolStripButton
            // 
            this.leftToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.leftToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.left_alignment;
            this.leftToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.leftToolStripButton.Name = "leftToolStripButton";
            this.leftToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.leftToolStripButton.Text = "Balra igazítás";
            this.leftToolStripButton.Click += new System.EventHandler(this.leftToolStripButton_Click);
            // 
            // centerToolStripButton
            // 
            this.centerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.centerToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.center_alignment;
            this.centerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.centerToolStripButton.Name = "centerToolStripButton";
            this.centerToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.centerToolStripButton.Text = "Középre igazítás";
            this.centerToolStripButton.Click += new System.EventHandler(this.centerToolStripButton_Click);
            // 
            // rightToolStripButton
            // 
            this.rightToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rightToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.right_alignment;
            this.rightToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rightToolStripButton.Name = "rightToolStripButton";
            this.rightToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.rightToolStripButton.Text = "Jobbra igazítás";
            this.rightToolStripButton.Click += new System.EventHandler(this.rightToolStripButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // boldToolStripButton
            // 
            this.boldToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.boldToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.bold;
            this.boldToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.boldToolStripButton.Name = "boldToolStripButton";
            this.boldToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.boldToolStripButton.Text = "Félkövér";
            this.boldToolStripButton.Click += new System.EventHandler(this.boldToolStripButton_Click);
            // 
            // italicToolStripButton
            // 
            this.italicToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.italicToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.italic;
            this.italicToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.italicToolStripButton.Name = "italicToolStripButton";
            this.italicToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.italicToolStripButton.Text = "Dőlt";
            this.italicToolStripButton.Click += new System.EventHandler(this.italicToolStripButton_Click);
            // 
            // underlineToolStripButton
            // 
            this.underlineToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.underlineToolStripButton.Image = global::Szakdolgozat_OBYBZK.Properties.Resources.underlined;
            this.underlineToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.underlineToolStripButton.Name = "underlineToolStripButton";
            this.underlineToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.underlineToolStripButton.Text = "Aláhúzott";
            this.underlineToolStripButton.Click += new System.EventHandler(this.underlineToolStripButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.fontToolStripMenuItem,
            this.paragraphToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1281, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.toolStripSeparator1,
            this.openToolStripMenuItem,
            this.openPdfToolStripMenuItem,
            this.openDocxToolStripMenuItem,
            this.toolStripSeparator9,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator10,
            this.saveAsPdfToolStripMenuItem,
            this.saveAsDocxFájlkéntToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "Fájl";
            this.fileToolStripMenuItem.DropDownOpened += new System.EventHandler(this.fileToolStripMenuItem_DropDownOpened);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.newToolStripMenuItem.Text = "Új";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(182, 6);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.openToolStripMenuItem.Text = "Megnyitás";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // openPdfToolStripMenuItem
            // 
            this.openPdfToolStripMenuItem.Name = "openPdfToolStripMenuItem";
            this.openPdfToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.openPdfToolStripMenuItem.Text = "PDF fájl megnyitása";
            this.openPdfToolStripMenuItem.Click += new System.EventHandler(this.openPdfToolStripMenuItem_Click);
            // 
            // openDocxToolStripMenuItem
            // 
            this.openDocxToolStripMenuItem.Name = "openDocxToolStripMenuItem";
            this.openDocxToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.openDocxToolStripMenuItem.Text = "Docx fájl megnyitása";
            this.openDocxToolStripMenuItem.Click += new System.EventHandler(this.openDocxToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(182, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.saveToolStripMenuItem.Text = "Mentés";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.saveAsToolStripMenuItem.Text = "Mentés másként";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(182, 6);
            // 
            // saveAsPdfToolStripMenuItem
            // 
            this.saveAsPdfToolStripMenuItem.Name = "saveAsPdfToolStripMenuItem";
            this.saveAsPdfToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.saveAsPdfToolStripMenuItem.Text = "Mentés PDF fájlként";
            this.saveAsPdfToolStripMenuItem.Click += new System.EventHandler(this.saveAsPdfToolStripMenuItem_Click);
            // 
            // saveAsDocxFájlkéntToolStripMenuItem
            // 
            this.saveAsDocxFájlkéntToolStripMenuItem.Name = "saveAsDocxFájlkéntToolStripMenuItem";
            this.saveAsDocxFájlkéntToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.saveAsDocxFájlkéntToolStripMenuItem.Text = "Mentés Docx fájlként";
            this.saveAsDocxFájlkéntToolStripMenuItem.Click += new System.EventHandler(this.saveAsDocxFájlkéntToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator8,
            this.insertImageToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.editToolStripMenuItem.Text = "Szerkesztés";
            this.editToolStripMenuItem.DropDownOpened += new System.EventHandler(this.editToolStripMenuItem_DropDownOpened);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.undoToolStripMenuItem.Text = "Visszavonás";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(145, 6);
            // 
            // insertImageToolStripMenuItem
            // 
            this.insertImageToolStripMenuItem.Name = "insertImageToolStripMenuItem";
            this.insertImageToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.insertImageToolStripMenuItem.Text = "Kép beszúrása";
            this.insertImageToolStripMenuItem.Click += new System.EventHandler(this.insertImageToolStripMenuItem_Click);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectFontToolStripMenuItem,
            this.toolStripSeparator2,
            this.fontColorToolStripMenuItem,
            this.toolStripSeparator3,
            this.boldToolStripMenuItem,
            this.italicToolStripMenuItem,
            this.underlineToolStripMenuItem,
            this.normalToolStripMenuItem,
            this.toolStripSeparator4,
            this.pageColorHáttérszínToolStripMenuItem});
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.fontToolStripMenuItem.Text = "Betűtípus";
            this.fontToolStripMenuItem.DropDownOpened += new System.EventHandler(this.fontToolStripMenuItem_DropDownOpened);
            // 
            // selectFontToolStripMenuItem
            // 
            this.selectFontToolStripMenuItem.Name = "selectFontToolStripMenuItem";
            this.selectFontToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.selectFontToolStripMenuItem.Text = "Betűtípus választása";
            this.selectFontToolStripMenuItem.Click += new System.EventHandler(this.selectFontToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(176, 6);
            // 
            // fontColorToolStripMenuItem
            // 
            this.fontColorToolStripMenuItem.Name = "fontColorToolStripMenuItem";
            this.fontColorToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.fontColorToolStripMenuItem.Text = "Betűszín";
            this.fontColorToolStripMenuItem.Click += new System.EventHandler(this.fontColorToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(176, 6);
            // 
            // boldToolStripMenuItem
            // 
            this.boldToolStripMenuItem.Name = "boldToolStripMenuItem";
            this.boldToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.boldToolStripMenuItem.Text = "Félkövér";
            this.boldToolStripMenuItem.Click += new System.EventHandler(this.boldToolStripMenuItem_Click);
            // 
            // italicToolStripMenuItem
            // 
            this.italicToolStripMenuItem.Name = "italicToolStripMenuItem";
            this.italicToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.italicToolStripMenuItem.Text = "Dőlt";
            this.italicToolStripMenuItem.Click += new System.EventHandler(this.italicToolStripMenuItem_Click);
            // 
            // underlineToolStripMenuItem
            // 
            this.underlineToolStripMenuItem.Name = "underlineToolStripMenuItem";
            this.underlineToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.underlineToolStripMenuItem.Text = "Aláhúzott";
            this.underlineToolStripMenuItem.Click += new System.EventHandler(this.underlineToolStripMenuItem_Click);
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.normalToolStripMenuItem.Text = "Alapértelmezett";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(176, 6);
            // 
            // pageColorHáttérszínToolStripMenuItem
            // 
            this.pageColorHáttérszínToolStripMenuItem.Name = "pageColorHáttérszínToolStripMenuItem";
            this.pageColorHáttérszínToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.pageColorHáttérszínToolStripMenuItem.Text = "Oldal háttérszín";
            this.pageColorHáttérszínToolStripMenuItem.Click += new System.EventHandler(this.pageColorHáttérszínToolStripMenuItem_Click);
            // 
            // paragraphToolStripMenuItem
            // 
            this.paragraphToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.behúzásToolStripMenuItem,
            this.igazításToolStripMenuItem});
            this.paragraphToolStripMenuItem.Name = "paragraphToolStripMenuItem";
            this.paragraphToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.paragraphToolStripMenuItem.Text = "Bekezdés";
            this.paragraphToolStripMenuItem.DropDownOpened += new System.EventHandler(this.paragraphToolStripMenuItem_DropDownOpened);
            // 
            // behúzásToolStripMenuItem
            // 
            this.behúzásToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indent0ToolStripMenuItem,
            this.indent5ToolStripMenuItem,
            this.indent10ToolStripMenuItem1,
            this.indent15ToolStripMenuItem2,
            this.indent20ToolStripMenuItem3});
            this.behúzásToolStripMenuItem.Name = "behúzásToolStripMenuItem";
            this.behúzásToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.behúzásToolStripMenuItem.Text = "Behúzás";
            // 
            // indent0ToolStripMenuItem
            // 
            this.indent0ToolStripMenuItem.Name = "indent0ToolStripMenuItem";
            this.indent0ToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.indent0ToolStripMenuItem.Text = "Nincs";
            this.indent0ToolStripMenuItem.Click += new System.EventHandler(this.indent0ToolStripMenuItem_Click);
            // 
            // indent5ToolStripMenuItem
            // 
            this.indent5ToolStripMenuItem.Name = "indent5ToolStripMenuItem";
            this.indent5ToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.indent5ToolStripMenuItem.Text = "5 pts";
            this.indent5ToolStripMenuItem.Click += new System.EventHandler(this.indent5ToolStripMenuItem_Click);
            // 
            // indent10ToolStripMenuItem1
            // 
            this.indent10ToolStripMenuItem1.Name = "indent10ToolStripMenuItem1";
            this.indent10ToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.indent10ToolStripMenuItem1.Text = "10 pts";
            this.indent10ToolStripMenuItem1.Click += new System.EventHandler(this.indent10ToolStripMenuItem1_Click);
            // 
            // indent15ToolStripMenuItem2
            // 
            this.indent15ToolStripMenuItem2.Name = "indent15ToolStripMenuItem2";
            this.indent15ToolStripMenuItem2.Size = new System.Drawing.Size(105, 22);
            this.indent15ToolStripMenuItem2.Text = "15 pts";
            this.indent15ToolStripMenuItem2.Click += new System.EventHandler(this.indent15ToolStripMenuItem2_Click);
            // 
            // indent20ToolStripMenuItem3
            // 
            this.indent20ToolStripMenuItem3.Name = "indent20ToolStripMenuItem3";
            this.indent20ToolStripMenuItem3.Size = new System.Drawing.Size(105, 22);
            this.indent20ToolStripMenuItem3.Text = "20 pts";
            this.indent20ToolStripMenuItem3.Click += new System.EventHandler(this.indent20ToolStripMenuItem3_Click);
            // 
            // igazításToolStripMenuItem
            // 
            this.igazításToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftToolStripMenuItem,
            this.centerToolStripMenuItem,
            this.rightToolStripMenuItem});
            this.igazításToolStripMenuItem.Name = "igazításToolStripMenuItem";
            this.igazításToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.igazításToolStripMenuItem.Text = "Igazítás";
            // 
            // leftToolStripMenuItem
            // 
            this.leftToolStripMenuItem.Name = "leftToolStripMenuItem";
            this.leftToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.leftToolStripMenuItem.Text = "Balra";
            this.leftToolStripMenuItem.Click += new System.EventHandler(this.leftToolStripMenuItem_Click);
            // 
            // centerToolStripMenuItem
            // 
            this.centerToolStripMenuItem.Name = "centerToolStripMenuItem";
            this.centerToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.centerToolStripMenuItem.Text = "Középre";
            this.centerToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // rightToolStripMenuItem
            // 
            this.rightToolStripMenuItem.Name = "rightToolStripMenuItem";
            this.rightToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.rightToolStripMenuItem.Text = "Jobbra";
            this.rightToolStripMenuItem.Click += new System.EventHandler(this.rightToolStripMenuItem_Click);
            // 
            // panelSpace
            // 
            this.panelSpace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(24)))), ((int)(((byte)(58)))));
            this.panelSpace.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSpace.Location = new System.Drawing.Point(259, 100);
            this.panelSpace.Name = "panelSpace";
            this.panelSpace.Size = new System.Drawing.Size(1281, 10);
            this.panelSpace.TabIndex = 2;
            // 
            // panelCurrentMenu
            // 
            this.panelCurrentMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(25)))), ((int)(((byte)(62)))));
            this.panelCurrentMenu.Controls.Add(this.labelWhichMenu);
            this.panelCurrentMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCurrentMenu.Location = new System.Drawing.Point(259, 0);
            this.panelCurrentMenu.Name = "panelCurrentMenu";
            this.panelCurrentMenu.Size = new System.Drawing.Size(1281, 100);
            this.panelCurrentMenu.TabIndex = 1;
            // 
            // labelWhichMenu
            // 
            this.labelWhichMenu.AutoSize = true;
            this.labelWhichMenu.Font = new System.Drawing.Font("Century Gothic", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWhichMenu.ForeColor = System.Drawing.Color.White;
            this.labelWhichMenu.Location = new System.Drawing.Point(10, 31);
            this.labelWhichMenu.Name = "labelWhichMenu";
            this.labelWhichMenu.Size = new System.Drawing.Size(141, 42);
            this.labelWhichMenu.TabIndex = 0;
            this.labelWhichMenu.Text = "Főoldal";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(33)))), ((int)(((byte)(74)))));
            this.ClientSize = new System.Drawing.Size(1540, 760);
            this.Controls.Add(this.panelMainSpace);
            this.Controls.Add(this.panelSpace);
            this.Controls.Add(this.panelCurrentMenu);
            this.Controls.Add(this.panelSideMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Epsilon";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelSideMenu.ResumeLayout(false);
            this.panelMainSpace.ResumeLayout(false);
            this.panelMainSpace.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPreview_2)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelCurrentMenu.ResumeLayout(false);
            this.panelCurrentMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private FontAwesome.Sharp.IconButton iconButtonWordProcessor;
        private FontAwesome.Sharp.IconButton iconButtonDocx;
        private FontAwesome.Sharp.IconButton iconButtonPdf;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelMainSpace;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton fontToolStripButton;
        private System.Windows.Forms.ToolStripButton colorToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton leftToolStripButton;
        private System.Windows.Forms.ToolStripButton centerToolStripButton;
        private System.Windows.Forms.ToolStripButton rightToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton boldToolStripButton;
        private System.Windows.Forms.ToolStripButton italicToolStripButton;
        private System.Windows.Forms.ToolStripButton underlineToolStripButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openPdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDocxToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem saveAsPdfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsDocxFájlkéntToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem insertImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectFontToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem fontColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem boldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem pageColorHáttérszínToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paragraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem behúzásToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indent0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indent5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indent10ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem indent15ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem indent20ToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem igazításToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightToolStripMenuItem;
        private System.Windows.Forms.Panel panelSpace;
        private System.Windows.Forms.Panel panelCurrentMenu;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label labelRoad;
        private System.Windows.Forms.PictureBox buttonPreview_2;
        private System.Windows.Forms.Button buttonSearchFile;
        private RoundTextBox textBoxRoad;
        private System.Windows.Forms.CheckBox checkBoxMetaXml;
        private System.Windows.Forms.CheckBox checkBoxMetaTxt;
        private System.Windows.Forms.CheckBox checkBoxGetMetadata;
        private System.Windows.Forms.CheckBox checkBoxDocxToTxt;
        private System.Windows.Forms.CheckBox checkBoxGetTable;
        private System.Windows.Forms.CheckBox checkBoxGetPictures;
        private System.Windows.Forms.CheckBox checkBoxGetPicturesFromDocx;
        private System.Windows.Forms.CheckBox checkBoxPdfToTxt;
        private System.Windows.Forms.CheckBox checkBoxDocxToPdf;
        private System.Windows.Forms.CheckBox checkBoxPdfToDocx;
        private System.Windows.Forms.Button buttonDo;
        private AxAcroPDFLib.AxAcroPDF axAcroPDF;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.Label labelWhichMenu;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.RichTextBox richTextBoxLoadDocx;
    }
}

