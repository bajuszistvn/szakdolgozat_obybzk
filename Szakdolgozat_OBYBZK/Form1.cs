﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Collections.Generic;
using FontAwesome.Sharp;
using GroupDocs.Parser;
using GroupDocs.Parser.Data;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using SautinSoft.Document;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;

namespace Szakdolgozat_OBYBZK
{
    public partial class Form1 : Form
    {
        /***** Változók *****/

        #region "Változók"

        private IconButton currentBtn;
        private Panel leftBorderBtn;

        private bool workWithPdf = false;
        private bool workWithDocx = false;

        private bool isPdf = false;
        private bool isDocx = false;

        private string currentFile;
        private string currentFilePath = "";

        #endregion

        /***** Változók vége *****/

        /***** Main Form betöltés *****/

        #region "Main Form betöltése"

        public Form1()
        {
            InitializeComponent();

            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new System.Drawing.Size(7, 208);
            panelSideMenu.Controls.Add(leftBorderBtn);
        }

        // A szoftver indításakor elvégzendő műveletek
        private void Form1_Load(object sender, EventArgs e)
        {
            currentFile = "";

            checkBoxPdfToTxt.Hide();
            checkBoxGetTable.Hide();
            checkBoxGetPictures.Hide();
            checkBoxGetMetadata.Hide();
            checkBoxMetaTxt.Hide();
            checkBoxMetaXml.Hide();
            checkBoxDocxToPdf.Hide();
            checkBoxDocxToTxt.Hide();
            checkBoxGetPicturesFromDocx.Hide();
            checkBoxPdfToDocx.Hide();
            richTextBoxLoadDocx.Hide();
            menuStrip1.Hide();
            toolStrip1.Hide();
            richTextBox1.Hide();
            labelRoad.Hide();
            textBoxRoad.Hide();
            buttonSearchFile.Hide();
            buttonPreview_2.Hide();
            buttonDo.Hide();
            axAcroPDF.Hide();

            labelWelcome.Show();

            checkBoxMetaTxt.Enabled = false;
            checkBoxMetaXml.Enabled = false;

            // "Előnézet gomb" hover kurzor
            buttonPreview_2.Cursor = Cursors.Hand;

            // A Docx előnézetet betöltő richTextBox tartalmának módosítását megakadályozó parancs
            richTextBoxLoadDocx.Enabled = false;
        }

        #endregion

        /***** Main Form betöltése vége *****/

        /***** Design *****/

        #region "Design"

        // Az éppen aktív funkcióválasztó gomb design módosításai
        private void ActivateButton(object senderBtn, System.Drawing.Color color)
        {
            if(senderBtn != null)
            {
                DisableButton();

                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = System.Drawing.Color.FromArgb(37, 36, 81);
                currentBtn.ForeColor = color;

                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new System.Drawing.Point(0, currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
            }
        }

        // Design színek mentése a funkcióválasztó gombokhoz
        private struct RGBcolors
        {
            public static System.Drawing.Color pdfColor = System.Drawing.Color.FromArgb(255, 33, 22);
            public static System.Drawing.Color docxColor = System.Drawing.Color.FromArgb(43, 102, 159);
            public static System.Drawing.Color wordProcessorColor = System.Drawing.Color.FromArgb(32, 114, 69);
        }

        // A nem kiválasztott funkciógomb design beállításai
        private void DisableButton()
        {
            if(currentBtn != null)
            {
                currentBtn.BackColor = System.Drawing.Color.FromArgb(31, 30, 68);
                currentBtn.ForeColor = System.Drawing.Color.Gainsboro;
            }
        }

        // A főoldal szerinti design betöltése
        private void Reset()
        {
            DisableButton();
            leftBorderBtn.Visible = false;
        }

        #endregion

        /***** Design vége *****/

        /***** Főfunkcióválasztó gombok működése *****/

        #region "Főfunkcióválasztók"

        // A PDF funkciógombot kiválasztva végbemenő változások
        private void iconButtonPdf_Click_1(object sender, EventArgs e)
        {

            ActivateButton(sender, RGBcolors.pdfColor);
            iconButtonPdf.BackgroundImage = Properties.Resources.pdfpng_menu;
            iconButtonDocx.BackgroundImage = Properties.Resources.wordpng_opa_menu;
            iconButtonWordProcessor.BackgroundImage = Properties.Resources.wordprocessor_menu_opa2;

            checkBoxDocxToPdf.Hide();
            checkBoxDocxToTxt.Hide();
            checkBoxGetPicturesFromDocx.Hide();
            richTextBox1.Hide();
            toolStrip1.Hide();
            menuStrip1.Hide();
            labelWelcome.Hide();
            richTextBoxLoadDocx.Hide();

            checkBoxPdfToDocx.Show();
            checkBoxPdfToTxt.Show();
            checkBoxGetTable.Show();
            checkBoxGetPictures.Show();
            checkBoxGetMetadata.Show();
            checkBoxMetaTxt.Show();
            checkBoxMetaXml.Show();
            buttonSearchFile.Show();
            buttonPreview_2.Show();
            buttonDo.Show();
            labelRoad.Show();
            textBoxRoad.Show();
            axAcroPDF.Show();

            labelWhichMenu.Text = "PDF fájlokkal végezhető műveletek";

            workWithPdf = true;

            workWithDocx = false;

            this.Text = "Epsilon";
        }

        // A DOCX funkciógombot kiválasztva végbemenő változások
        private void iconButtonDocx_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBcolors.docxColor);
            iconButtonPdf.BackgroundImage = Properties.Resources.pdfpng_opa_menu;
            iconButtonDocx.BackgroundImage = Properties.Resources.wordpng_menu_3;
            iconButtonWordProcessor.BackgroundImage = Properties.Resources.wordprocessor_menu_opa2;

            checkBoxPdfToDocx.Hide();
            checkBoxPdfToTxt.Hide();
            checkBoxGetTable.Hide();
            checkBoxGetPictures.Hide();
            checkBoxGetMetadata.Hide();
            checkBoxMetaTxt.Hide();
            checkBoxMetaXml.Hide();
            richTextBox1.Hide();
            toolStrip1.Hide();
            menuStrip1.Hide();
            labelWelcome.Hide();
            axAcroPDF.Hide();

            checkBoxDocxToPdf.Show();
            checkBoxDocxToTxt.Show();
            checkBoxGetPicturesFromDocx.Show();
            buttonSearchFile.Show();
            buttonPreview_2.Show();
            buttonDo.Show();
            labelRoad.Show();
            textBoxRoad.Show();
            richTextBoxLoadDocx.Show();

            labelWhichMenu.Text = "DOCX fájlokkal végezhető műveletek";

            workWithDocx = true;

            workWithPdf = false;

            this.Text = "Epsilon";
        }

        // A szövegszerkesztő funkciógombot kiválasztva végbemenő változások
        private void iconButtonWordProcessor_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBcolors.wordProcessorColor);
            iconButtonPdf.BackgroundImage = Properties.Resources.pdfpng_opa_menu;
            iconButtonDocx.BackgroundImage = Properties.Resources.wordpng_opa_menu;
            iconButtonWordProcessor.BackgroundImage = Properties.Resources.wordprocessor_menu;

            checkBoxPdfToDocx.Hide();
            checkBoxPdfToTxt.Hide();
            checkBoxGetTable.Hide();
            checkBoxGetPictures.Hide();
            checkBoxGetMetadata.Hide();
            checkBoxMetaTxt.Hide();
            checkBoxMetaXml.Hide();
            checkBoxDocxToPdf.Hide();
            checkBoxDocxToTxt.Hide();
            checkBoxGetPicturesFromDocx.Hide();
            buttonSearchFile.Hide();
            buttonPreview_2.Hide();
            buttonDo.Hide();
            labelRoad.Hide();
            textBoxRoad.Hide();
            axAcroPDF.Hide();
            labelWelcome.Hide();
            richTextBoxLoadDocx.Hide();

            richTextBox1.Show();
            toolStrip1.Show();
            menuStrip1.Show();

            labelWhichMenu.Text = "Szövegszerkesztő";

            workWithPdf = false;
            workWithDocx = false;

            this.Text = "Epsilon";
        }

        // A "Home" gombra kattintva végbemenő változtatások
        private void panelLogo_Click(object sender, EventArgs e)
        {
            Reset();

            iconButtonPdf.BackgroundImage = Properties.Resources.pdfpng_menu;
            iconButtonDocx.BackgroundImage = Properties.Resources.wordpng_menu_3;
            iconButtonWordProcessor.BackgroundImage = Properties.Resources.wordprocessor_menu;

            checkBoxPdfToDocx.Hide();
            checkBoxPdfToTxt.Hide();
            checkBoxGetTable.Hide();
            checkBoxGetPictures.Hide();
            checkBoxGetMetadata.Hide();
            checkBoxMetaTxt.Hide();
            checkBoxMetaXml.Hide();
            checkBoxDocxToPdf.Hide();
            checkBoxDocxToTxt.Hide();
            checkBoxGetPicturesFromDocx.Hide();
            richTextBox1.Hide();
            toolStrip1.Hide();
            menuStrip1.Hide();
            buttonSearchFile.Hide();
            buttonPreview_2.Hide();
            buttonDo.Hide();
            labelRoad.Hide();
            textBoxRoad.Hide();
            axAcroPDF.Hide();         
            richTextBoxLoadDocx.Hide();

            labelWelcome.Show();

            labelWhichMenu.Text = "Főoldal";

            workWithDocx = false;

            workWithPdf = false;

            this.Text = "Epsilon";
        }

        #endregion

        /***** Főfunkcióválasztó gombok működése vége *****/

        /***** Checkbox *****/

        #region "Checkbox"

        // Metaadatok gyűjtése esetén engedélyezze a két checkbox-ot
        private void checkBoxGetMetadata_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBoxGetMetadata.Checked == true)
            {
                checkBoxMetaTxt.Enabled = true;
                checkBoxMetaXml.Enabled = true;
            }
            else
            {
                checkBoxMetaTxt.Enabled = false;
                checkBoxMetaXml.Enabled = false;
            }
        }
        
        #endregion
        
        /***** Checkbox vége *****/

        /***** Gombok működése *****/

        #region "Gombok működése"

        // A "Tallózás" gomb működése
        private void buttonSearchFile_Click(object sender, EventArgs e)
        {
            try
            {
                // Ha PDF van kiválasztva, akkor a "Tallózás" gomb csak PDF fájlokat fog listázni
                if (workWithPdf == true)
                {
                    using (OpenFileDialog ofd = new OpenFileDialog() { ValidateNames = true, Multiselect = false, Filter = "PDF|*.pdf" })
                    {
                        if (ofd.ShowDialog() == DialogResult.OK)
                        {
                            axAcroPDF.src = ofd.FileName;
                            textBoxRoad.Text = ofd.FileName;
                            currentFilePath = ofd.FileName;
                        }
                    }
                }
                // Ha Docx van kiválasztva, akkor a "Tallózás" gomb csak docx-et fog listázni
                else if (workWithDocx == true)
                {
                    using (OpenFileDialog ofd = new OpenFileDialog() { ValidateNames = true, Multiselect = false, Filter = "DOCX|*.docx|DOC|*.doc" })
                    {
                        if (ofd.ShowDialog() == DialogResult.OK)
                        {
                            textBoxRoad.Text = ofd.FileName;
                            currentFilePath = ofd.FileName;

                            // Docx fájl előnézetének betöltése

                            string inpFile = textBoxRoad.Text;
                            string outFile = @"Result.rtf";

                            DocumentCore dc = DocumentCore.Load(inpFile);
                            dc.Save(outFile);

                            richTextBoxLoadDocx.LoadFile(outFile, RichTextBoxStreamType.RichText);

                            File.Delete(outFile);

                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("A fájl megnyitása közben probléma lépett fel!", "Sikertelen megnyitás");
            }
        }

        // Az "Előnézet" gomb működése
        private void buttonPreview_2_Click(object sender, EventArgs e)
        {
            try
            {
                // PDF fájl előnézetének betöltése
                if (workWithPdf == true)
                {
                    axAcroPDF.src = textBoxRoad.Text;
                    currentFilePath = textBoxRoad.Text;
                }
                // Docx fájl előnézetének betöltése
                else if (workWithDocx == true)
                {
                    string inpFile = textBoxRoad.Text;
                    string outFile = @"Result.rtf";

                    DocumentCore dc = DocumentCore.Load(inpFile);
                    dc.Save(outFile);

                    richTextBoxLoadDocx.LoadFile(outFile, RichTextBoxStreamType.RichText);

                    File.Delete(outFile);

                    currentFilePath = textBoxRoad.Text;
                }
            }
            catch
            {
                MessageBox.Show("A fájl előnézetének betöltése közben hiba lépett fel!", "Sikertelen előnézet betöltés");
            }
        }

        // A "Véghrehajt" gomb működése
        private void buttonDo_Click(object sender, EventArgs e)
        {
            if (workWithPdf == true)
            {
                // Ha PDF fájl a bemenet
                if ((checkBoxPdfToDocx.Checked == false && checkBoxGetPictures.Checked == false && checkBoxGetTable.Checked == false && checkBoxPdfToTxt.Checked == false && checkBoxGetMetadata.Checked == false/*pdfToDocx == false && getPictures == false && getTable == false && pdfToTxt == false && getMetadata == false*/))
                {
                    warningNoJobMsgForm emsg = new warningNoJobMsgForm();
                    emsg.Show();
                }
                else if (textBoxRoad.Text == "" || GetLast(textBoxRoad.Text, 4) != ".pdf")
                {
                    warningBadTypeMsgForm wmsg = new warningBadTypeMsgForm();
                    wmsg.Show();
                }
                else
                {
                    // Ellenőrzi, hogy a megfelelő PDF van-e előtöltve, ha nem, akkor lecseréli a helyesre
                    if (currentFilePath != textBoxRoad.Text)
                    {
                        axAcroPDF.src = textBoxRoad.Text;
                        currentFilePath = textBoxRoad.Text;
                    }

                    bool anyError = false;

                    string filePath = textBoxRoad.Text;

                    loadingForm lform = new loadingForm();
                    lform.Show();

                    try
                    {
                        // Ha a PDF fájl docx-re konvertálása ki volt jelölve, akkor végrehajtja
                        if (checkBoxPdfToDocx.Checked)
                        {
                            string rndString = randomFileName(filePath, "docx", false);

                            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

                            f.OpenPdf(@filePath);

                            if (f.PageCount > 0)
                            {
                                    f.WordOptions.Format = SautinSoft.PdfFocus.CWordOptions.eWordDocument.Docx;
                                    f.ToWord(@rndString);
                            }
                        }

                        // Ha a képek mentése ki volt választva, akkor végrehajtja
                        if (checkBoxGetPictures.Checked)
                        {
                            using (Parser parser = new Parser(filePath))
                            {
                                IEnumerable<PageImageArea> images = parser.GetImages();

                                if (images == null)
                                {
                                    return;
                                }

                                int counter = 1;

                                foreach (PageImageArea image in images)
                                {
                                    string rndString = RandomString(5);
                                    Image.FromStream(image.GetImageStream()).Save(string.Format(filePath + "_" + rndString + "{0}.Jpeg", counter++), ImageFormat.Jpeg);
                                }
                            }
                        }

                        // Ha a Metadata mentése ki volt választva, akkor végrehajtja
                        // Adatok: author, creator, creation date, keywords, modification date, producer, subject, title
                        if (checkBoxGetMetadata.Checked/*getMetadata == true*/)
                        {
                            // Metaadatok TxT fájlba való kiírása
                            if (checkBoxMetaTxt.Checked)
                            {
                                try
                                {
                                    string newFilePath = randomFileName(filePath, "txt", true);

                                    string datas = "";
                                    PdfDocument document = PdfReader.Open(filePath);

                                    datas = "Author: " + document.Info.Author + "; " + Environment.NewLine + "Creator: " + document.Info.Creator + "; " + Environment.NewLine + "Creation Date: " + document.Info.CreationDate +
                                            "; " + Environment.NewLine + "Keywords: " + document.Info.Keywords + ";" + Environment.NewLine + "Modification Date: " + document.Info.ModificationDate +
                                            "; " + Environment.NewLine + "Producer: " + document.Info.Producer + "; " + Environment.NewLine + "Subject: " + document.Info.Subject +
                                            "; " + Environment.NewLine + "Title: " + document.Info.Title;

                                    try
                                    {
                                        // Létrehozza az új TXT fájlt
                                        using (FileStream fs = File.Create(newFilePath))
                                        {
                                            byte[] metaText = new UTF8Encoding(true).GetBytes(datas);
                                            fs.Write(metaText, 0, metaText.Length);
                                        }
                                    }
                                    catch
                                    {
                                        errorMsgForm emsg = new errorMsgForm();
                                        emsg.Show();
                                    }
                                }
                                catch
                                {
                                    using (StreamWriter writer = File.CreateText(filePath + ".txt"))
                                    {
                                        writer.WriteLine("Nem lehetett metaadatot szerezni ebből a fájlból.");
                                    }
                                }
                            }
                            // PDF metaadatainak XML fájlba mentése
                            if (checkBoxMetaXml.Checked)
                            {
                                try
                                {
                                    string newFilePath = randomFileName(filePath, "xml", false);

                                    PdfDocument document = PdfReader.Open(filePath);

                                    string cr_date_str = document.Info.CreationDate.ToString("dd/MM/yyyy HH:mm:ss");
                                    string md_date_str = document.Info.ModificationDate.ToString("dd/MM/yyyy HH:mm:ss");

                                    // Az XML dokumentum kinézetének megadása
                                    XmlWriterSettings settings = new XmlWriterSettings();
                                    settings.Indent = true;
                                    settings.IndentChars = ("    ");
                                    settings.CloseOutput = true;
                                    settings.OmitXmlDeclaration = true;

                                    using (XmlWriter writer = XmlWriter.Create(newFilePath, settings))
                                    {
                                        writer.WriteStartElement("metadata");
                                        writer.WriteElementString("author", document.Info.Author);
                                        writer.WriteElementString("creator", document.Info.Creator);
                                        writer.WriteElementString("creationDate", cr_date_str);
                                        writer.WriteElementString("keywords", document.Info.Keywords);
                                        writer.WriteElementString("modificationDate", md_date_str);
                                        writer.WriteElementString("producer", document.Info.Producer);
                                        writer.WriteElementString("subject", document.Info.Subject);
                                        writer.WriteElementString("title", document.Info.Title);
                                        writer.WriteEndElement();
                                        writer.Flush();
                                    }
                                }
                                catch
                                {
                                    using (StreamWriter writer = File.CreateText(filePath + ".txt"))
                                    {
                                        writer.WriteLine("Nem lehetett metaadatot szerezni ebből a fájlból.");
                                    }
                                }
                            }
                        }

                        // Ha a táblázat mentése ki volt választva, akkor végrehajtja
                        if (checkBoxGetTable.Checked)
                        {
                            string newFilePath = randomFileName(filePath, "xls", false);

                            string pathToExcel = Path.ChangeExtension(filePath, ".xls");
                            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

                            f.ExcelOptions.ConvertNonTabularDataToSpreadsheet = false;
                            f.ExcelOptions.PreservePageLayout = true;

                            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("hu-HU");

                            ci.NumberFormat.NumberDecimalSeparator = ",";
                            ci.NumberFormat.NumberGroupSeparator = ".";

                            f.ExcelOptions.CultureInfo = ci;

                            f.OpenPdf(filePath);

                            if (f.PageCount > 0)
                            {
                                int result = f.ToExcel(pathToExcel);
                            }
                        }

                        // PDF fájl TXT formátummá konvertálása
                        if (checkBoxPdfToTxt.Checked)
                        {
                            string pathToTxt = Path.ChangeExtension(filePath, ".txt");
                            string rndString = RandomString(5);
                            pathToTxt = randomFileName(filePath, "txt", false);

                            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

                            f.OpenPdf(filePath);
                            f.ToText(pathToTxt);
                        }
                    }
                    catch
                    {
                        lform.Hide();

                        anyError = true;
                        errorMsgForm emsg = new errorMsgForm();
                        emsg.Show();
                    }

                    if (anyError == false)
                    {
                        lform.Hide();

                        successMsgForm msg = new successMsgForm();
                        msg.Show();
                    }
                }
            }
            // Ha Docx fájl a bemenet
            else if (workWithDocx == true)
            {
                if (checkBoxDocxToPdf.Checked == false && checkBoxDocxToTxt.Checked == false && checkBoxGetPicturesFromDocx.Checked == false/*(docxToPdf == false && docxToTxt == false && getPicturesFromDocx == false)*/)
                {
                    warningNoJobMsgForm emsg = new warningNoJobMsgForm();
                    emsg.Show();
                }
                else if (textBoxRoad.Text == "" || (GetLast(textBoxRoad.Text, 5) != ".docx" && GetLast(textBoxRoad.Text, 4) != ".doc"))
                {
                    warningBadTypeMsgForm wmsg = new warningBadTypeMsgForm();
                    wmsg.Show();
                }
                else
                {
                    if(currentFilePath != textBoxRoad.Text)
                    {
                        string inpFile = textBoxRoad.Text;
                        string outFile = @"Result.rtf";

                        DocumentCore dc = DocumentCore.Load(inpFile);
                        dc.Save(outFile);

                        richTextBoxLoadDocx.LoadFile(outFile, RichTextBoxStreamType.RichText);

                        File.Delete(outFile);
                    }

                    bool anyError = false;
                    string filePath = textBoxRoad.Text;

                    loadingForm lform = new loadingForm();
                    lform.Show();

                    try
                    {
                        // Ha a Docx fájl PDF fájlá kovertálás ki volt választva
                        if (checkBoxDocxToPdf.Checked)
                        {
                            string newFile = randomFileName(filePath, "pdf", false);
                            DocumentCore dc = DocumentCore.Load(@filePath);
                            dc.Save(newFile);
                        }

                        // Ha a Docx fájl TxT fájlá konvertálása ki volt választva
                        if (checkBoxDocxToTxt.Checked)
                        {
                            string inpFile = filePath;
                            string newFile = randomFileName(filePath, "txt", false);

                            DocumentCore dc = DocumentCore.Load(inpFile);
                            dc.Save(newFile);
                        }

                        // Ha a Képek mentése Docx fájlból ki volt választva
                        if (checkBoxGetPicturesFromDocx.Checked)
                        {
                            Document document = new Document(filePath);
                            int index = 0;

                            foreach (Spire.Doc.Section section in document.Sections)
                            {
                                foreach (Spire.Doc.Documents.Paragraph paragraph in section.Paragraphs)
                                {
                                    foreach (DocumentObject docObject in paragraph.ChildObjects)
                                    {
                                        if (docObject.DocumentObjectType == DocumentObjectType.Picture)
                                        {
                                            DocPicture picture = docObject as DocPicture;
                                            string rndString = RandomString(5);
                                            String imageName = String.Format(filePath + "_" + rndString + "{0}.Jpeg", index);
                                            picture.Image.Save(imageName, System.Drawing.Imaging.ImageFormat.Png);
                                            index++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        lform.Hide();

                        anyError = true;
                        errorMsgForm emsg = new errorMsgForm();
                        emsg.Show();
                    }

                    if (anyError == false)
                    {
                        lform.Hide();

                        successMsgForm msg = new successMsgForm();
                        msg.Show();
                    }
                }
            }
        }

        #endregion

        /***** Gombok működése vége *****/

        /***** Egyéb függvények *****/

        #region "Egyéb függvények"

        // Egy string utolsó X karaktere
        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }

        // Random String generálása    
        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        // Random név generálása a létrehozott fájloknak kiterjesztés módosítással
        // A random névadáshoz a RandomString függvényt használja
        // Kiterjesztések amikre módosítani kell: xls, xml, txt
        // docx és jpg esetén nincs szükség rá, rövid megoldással meg tudtam oldani
        public static string randomFileName(string path, string extension, bool isMeta)
        {
            bool isNameTaken;
            string rndString;

            switch (extension)
            {
                case "xls":
                    do
                    {
                        rndString = RandomString(5);
                        rndString = path + "_tables_" + rndString + ".xls";

                        if (File.Exists(rndString))
                        {
                            isNameTaken = true;
                        }
                        else
                        {
                            isNameTaken = false;
                        }

                    } while (isNameTaken);

                    return rndString;

                case "xml":
                    do
                    {
                        rndString = RandomString(5);
                        rndString = path + "_metadata_XML_" + rndString + ".xml";

                        if (File.Exists(rndString))
                        {
                            isNameTaken = true;
                        }
                        else
                        {
                            isNameTaken = false;
                        }

                    } while (isNameTaken);

                    return rndString;

                case "txt":
                    do
                    {
                        rndString = RandomString(5);

                        if (isMeta)
                        {
                            rndString = path + "metadata_txt_" + rndString + ".txt";
                        }
                        else
                        {
                            rndString = path + "_txt_" + rndString + ".txt";
                        }

                        if (File.Exists(rndString))
                        {
                            isNameTaken = true;
                        }
                        else
                        {
                            isNameTaken = false;
                        }

                    } while (isNameTaken);

                    return rndString;

                case "pdf":
                    do
                    {
                        rndString = RandomString(5);
                        rndString = path + rndString + ".pdf";

                        if (File.Exists(rndString))
                        {
                            isNameTaken = true;
                        }
                        else
                        {
                            isNameTaken = false;
                        }

                    } while (isNameTaken);

                    return rndString;

                case "docx":
                    do
                    {
                        rndString = RandomString(5);
                        rndString = path + rndString + ".docx";

                        if (File.Exists(rndString))
                        {
                            isNameTaken = true;
                        }
                        else
                        {
                            isNameTaken = false;
                        }

                    } while (isNameTaken);

                    return rndString;

                default:
                    return "Error";
            }
        }

        #endregion

        /***** Egyéb függvények vége *****/

        /***** Szövegszerkesztő *****/

        #region "Szövegszerkesztő"

        // Fájl -> Új működése
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (richTextBox1.Modified == true)
                {
                    System.Windows.Forms.DialogResult answer;
                    answer = MessageBox.Show("Elmenti a változtatásokat?", "Mentetlen változtatások", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (answer == System.Windows.Forms.DialogResult.No)
                    {
                        currentFile = "";
                        richTextBox1.Modified = false;
                        richTextBox1.Clear();
                        richTextBox1.BackColor = System.Drawing.Color.White;

                        isPdf = false;
                        isDocx = false;

                        return;
                    }
                    else
                    {
                        saveToolStripMenuItem_Click(this, new EventArgs());
                        richTextBox1.Modified = false;
                        richTextBox1.Clear();
                        currentFile = "";
                        richTextBox1.BackColor = System.Drawing.Color.White;

                        isPdf = false;
                        isDocx = false;

                        return;
                    }
                }
                else
                {
                    currentFile = "";
                    richTextBox1.Modified = false;
                    richTextBox1.Clear();
                    richTextBox1.BackColor = System.Drawing.Color.White;

                    isPdf = false;
                    isDocx = false;

                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba:");
            }
        }

        // Fájl -> Megnyitás működése
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (richTextBox1.Modified == true)
                {
                    System.Windows.Forms.DialogResult answer;
                    answer = MessageBox.Show("Elmenti a változtatásokat?", "Mentetlen változtatások", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (answer == System.Windows.Forms.DialogResult.No)
                    {
                        richTextBox1.Modified = false;
                        OpenFile();
                    }
                    else
                    {
                        saveToolStripMenuItem_Click(this, new EventArgs());
                        OpenFile();
                    }
                }
                else
                {
                    OpenFile();
                }

                isPdf = false;
                isDocx = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba:");
            }
        }

        // Fájl -> PDF fájl megnyitása működése
        private void openPdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "PDF megnyitás";
                openFileDialog1.DefaultExt = "rtf";
                openFileDialog1.Filter = "PDF|*.pdf";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.FileName = string.Empty;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog1.FileName == "")
                    {
                        return;
                    }

                    string strExt;
                    strExt = System.IO.Path.GetExtension(openFileDialog1.FileName);
                    strExt = strExt.ToUpper();

                    if (strExt == ".PDF")
                    {
                        string inpFile = openFileDialog1.FileName;
                        string outFile = @"Result.rtf";

                        PdfLoadOptions pdfLO = new PdfLoadOptions()
                        {
                            RasterizeVectorGraphics = false,
                            DetectTables = true,
                            PreserveEmbeddedFonts = PropertyState.Auto
                        };

                        DocumentCore dc = DocumentCore.Load(inpFile, pdfLO);

                        dc.Save(outFile);

                        richTextBox1.LoadFile(outFile, RichTextBoxStreamType.RichText);

                        File.Delete(outFile);
                    }
                }

                isPdf = true;
                isDocx = false;
            }
            catch
            {
                MessageBox.Show("A fájl megnyitása közben probléma lépett fel!", "Sikertelen megnyitás");
            }
        }

        // Fájl -> DOCX fájl megnyitása működése
        private void openDocxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Title = "Docx megnyitás";
                openFileDialog1.DefaultExt = "rtf";
                openFileDialog1.Filter = "DOCX|*.docx";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.FileName = string.Empty;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog1.FileName == "")
                    {
                        return;
                    }

                    string strExt;
                    strExt = System.IO.Path.GetExtension(openFileDialog1.FileName);
                    strExt = strExt.ToUpper();

                    if (strExt == ".DOCX")
                    {
                        string inpFile = openFileDialog1.FileName;
                        string outFile = @"Result.rtf";

                        DocumentCore dc = DocumentCore.Load(inpFile);
                        dc.Save(outFile);

                        richTextBox1.LoadFile(outFile, RichTextBoxStreamType.RichText);

                        File.Delete(outFile);
                    }

                }

                isPdf = false;
                isDocx = true;
            }
            catch
            {
                MessageBox.Show("A fájl megnyitása közben probléma lépett fel!", "Sikertelen megnyitás");
            }
        }

        // Fájl -> Mentés működése
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (isPdf == true)
                {
                    saveAsPdfToolStripMenuItem_Click(this, e);
                }
                else if (isDocx == true)
                {
                    saveAsDocxFájlkéntToolStripMenuItem_Click(this, e);
                }
                else
                {
                    if (currentFile == string.Empty)
                    {
                        saveAsToolStripMenuItem_Click(this, e);
                        return;
                    }

                    try
                    {
                        string strExt;
                        strExt = System.IO.Path.GetExtension(currentFile);
                        strExt = strExt.ToUpper();

                        if (strExt == ".RTF")
                        {
                            richTextBox1.SaveFile(currentFile);
                        }
                        else
                        {
                            System.IO.StreamWriter txtWriter;
                            txtWriter = new System.IO.StreamWriter(currentFile);
                            txtWriter.Write(richTextBox1.Text);
                            txtWriter.Close();
                            txtWriter = null;
                            richTextBox1.SelectionStart = 0;
                            richTextBox1.SelectionLength = 0;
                        }

                        this.Text = "Epsilon: " + currentFile.ToString();
                        richTextBox1.Modified = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString(), "Hiba a mentés közben!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba:");
            }
        }

        // Fájl -> Mentés másként működése
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.Title = "Mentés másként";
                saveFileDialog1.DefaultExt = "rtf";
                saveFileDialog1.Filter = "Rich Text|*.rtf|TXT|*.txt|HTML|*.htm|Minden fájl|*.*";
                saveFileDialog1.FilterIndex = 1;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {

                    if (saveFileDialog1.FileName == "")
                    {
                        return;
                    }

                    string strExt;
                    strExt = System.IO.Path.GetExtension(saveFileDialog1.FileName);
                    strExt = strExt.ToUpper();

                    if (strExt == ".RTF")
                    {
                        richTextBox1.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.RichText);
                    }
                    else
                    {
                        System.IO.StreamWriter txtWriter;
                        txtWriter = new System.IO.StreamWriter(saveFileDialog1.FileName);
                        txtWriter.Write(richTextBox1.Text);
                        txtWriter.Close();
                        txtWriter = null;
                        richTextBox1.SelectionStart = 0;
                        richTextBox1.SelectionLength = 0;
                    }

                    currentFile = saveFileDialog1.FileName;
                    richTextBox1.Modified = false;
                    this.Text = "Epsilon: " + currentFile.ToString();
                    MessageBox.Show(currentFile.ToString() + " sikeresen mentve.", "Dokumentum elmentve");

                    isPdf = false;
                    isDocx = false;
                }
                else
                {
                    MessageBox.Show("A mentés megszakítva a felhasználó által.", "Megszakítva");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Fájl -> Mentés PDF fájlként működése
        private void saveAsPdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string desktopPathRtf = (Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) + "\\pdfResult" + RandomString(6) + ".rtf";
                string desktopPathDocx = (Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) + "\\pdfResult" + RandomString(6) + ".docx";
                string desktopPathPdf = (Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) + "\\pdfResult" + RandomString(6) + ".pdf";

                using (File.Create(@desktopPathRtf)) ;
                richTextBox1.SaveFile(@desktopPathRtf, RichTextBoxStreamType.RichText);

                string inpFile = @desktopPathRtf;
                string outFile = @desktopPathDocx;

                DocumentCore dc = DocumentCore.Load(inpFile);
                dc.Save(outFile);

                inpFile = @desktopPathDocx;
                outFile = @desktopPathPdf;

                dc = DocumentCore.Load(inpFile);
                dc.Save(outFile);

                File.Delete(@desktopPathRtf);
                File.Delete(@desktopPathDocx);

                MessageBox.Show("A fájl mentésre került a " + desktopPathPdf + " helyre.");
            }
            catch
            {
                MessageBox.Show("A fájl létrehozás közben probléma lépett fel!", "Sikertelen mentés");
            }
        }

        // Fájl -> Mentés DOCX fájlként működése
        private void saveAsDocxFájlkéntToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string desktopPathRtf = (Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) + "\\pdfResult" + RandomString(6) + ".rtf";
                string desktopPathDocx = (Environment.GetFolderPath(Environment.SpecialFolder.Desktop)) + "\\pdfResult" + RandomString(6) + ".docx";

                using (File.Create(@desktopPathRtf)) ;
                richTextBox1.SaveFile(@desktopPathRtf, RichTextBoxStreamType.RichText);

                string inpFile = @desktopPathRtf;
                string outFile = @desktopPathDocx;

                DocumentCore dc = DocumentCore.Load(inpFile);
                dc.Save(outFile);

                File.Delete(@desktopPathRtf);

                MessageBox.Show("A fájl mentésre került a " + desktopPathDocx + " helyre.");
            }
            catch
            {
                MessageBox.Show("A fájl létrehozás közben probléma lépett fel!", "Sikertelen mentés");
            }
        }

        // Szerkesztés -> Visszavonás működése
        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (richTextBox1.CanUndo)
                {
                    richTextBox1.Undo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }
        
        // Szerkesztés -> Redo működése
        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (richTextBox1.CanRedo)
                {
                    richTextBox1.Redo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Szerkesztés -> Kép beszúrása működése
        private void insertImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Kép beszúrása";
            openFileDialog1.DefaultExt = "rtf";
            openFileDialog1.Filter = "JPEG|*.jpeg|JPG|*.jpg|PNG|*.png|Bitmap|*.bmp|GIF|*.gif";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.ShowDialog();

            if (openFileDialog1.FileName == "")
            {
                return;
            }

            try
            {
                string strImagePath = openFileDialog1.FileName;
                Image img;
                img = Image.FromFile(strImagePath);
                Clipboard.SetDataObject(img);
                DataFormats.Format df;
                df = DataFormats.GetFormat(DataFormats.Bitmap);
                if (this.richTextBox1.CanPaste(df))
                {
                    this.richTextBox1.Paste(df);
                }
            }
            catch
            {
                MessageBox.Show("A választott képet nem lehet beszúrni.", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Betűtípus -> Betűtípus választása működése
        private void selectFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(richTextBox1.SelectionFont == null))
                {
                    fontDialog1.Font = richTextBox1.SelectionFont;
                }
                else
                {
                    fontDialog1.Font = null;
                }
                fontDialog1.ShowApply = true;
                if (fontDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    richTextBox1.SelectionFont = fontDialog1.Font;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Betűtípus -> Betűszín működése
        private void fontColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                colorDialog1.Color = richTextBox1.ForeColor;
                if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    richTextBox1.SelectionColor = colorDialog1.Color;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Betűtípus -> Félkövér működése
        private void boldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(richTextBox1.SelectionFont == null))
                {
                    System.Drawing.Font currentFont = richTextBox1.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;

                    newFontStyle = richTextBox1.SelectionFont.Style ^ FontStyle.Bold;

                    richTextBox1.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Betűtípus -> Dőlt működése
        private void italicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(richTextBox1.SelectionFont == null))
                {
                    System.Drawing.Font currentFont = richTextBox1.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;

                    newFontStyle = richTextBox1.SelectionFont.Style ^ FontStyle.Italic;

                    richTextBox1.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Betűtípus -> Aláhúzott működése
        private void underlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(richTextBox1.SelectionFont == null))
                {
                    System.Drawing.Font currentFont = richTextBox1.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;

                    newFontStyle = richTextBox1.SelectionFont.Style ^ FontStyle.Underline;

                    richTextBox1.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Betűtípus -> Alapértelmezett működése
        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(richTextBox1.SelectionFont == null))
                {
                    System.Drawing.Font currentFont = richTextBox1.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;
                    newFontStyle = FontStyle.Regular;
                    richTextBox1.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }
        
        // A kivjelölt rész módosítása
        private void richTextBox1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                boldToolStripButton.Checked = richTextBox1.SelectionFont.Bold;
                italicToolStripButton.Checked = richTextBox1.SelectionFont.Italic;
                underlineToolStripButton.Checked = richTextBox1.SelectionFont.Underline;
            }
            catch
            {
                MessageBox.Show("Hiba");
            }
        }

        // Betűtípus -> Oldal háttérszín működése
        private void pageColorHáttérszínToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                colorDialog1.Color = richTextBox1.BackColor;
                if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    richTextBox1.BackColor = colorDialog1.Color;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Behúzás -> Nincs működése
        private void indent0ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionIndent = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Behúzás -> 5 pts működése
        private void indent5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionIndent = 5;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Behúzás -> 10 pts működése
        private void indent10ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionIndent = 10;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Behúzás -> 15 pts működése
        private void indent15ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionIndent = 15;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Behúzás -> 20 pts működése
        private void indent20ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionIndent = 20;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Igazítás -> Balra működése
        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Left;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Igazítás -> Középre működése
        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Bekezdés -> Igazítás -> Jobbra működése
        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Right;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // Új gyorsgomb működése
        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            newToolStripMenuItem_Click(this, e);
        }

        // Megnyitás gyorsgomb működése
        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            openToolStripMenuItem_Click(this, e);
        }

        // Mentés gyorsgomb működése
        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            saveToolStripMenuItem_Click(this, e);
        }

        // Betűtípus gyorsgomb működése
        private void fontToolStripButton_Click(object sender, EventArgs e)
        {
            selectFontToolStripMenuItem_Click(this, e);
        }

        // Betűszín gyorsgomb működése
        private void colorToolStripButton_Click(object sender, EventArgs e)
        {
            fontColorToolStripMenuItem_Click(this, new EventArgs());
        }

        // Balra igazítás gyorsgomb működése
        private void leftToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Left;
        }

        // Középre igazítás gyorsgomb működése
        private void centerToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
        }

        // Jobbra gyorsgomb működése
        private void rightToolStripButton_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Right;
        }

        // Félkövér gyorsgomb működése
        private void boldToolStripButton_Click(object sender, EventArgs e)
        {
            boldToolStripMenuItem_Click(this, e);
        }

        // Dőlt gyorsgomb működése
        private void italicToolStripButton_Click(object sender, EventArgs e)
        {
            italicToolStripMenuItem_Click(this, e);
        }

        // Aláhúzott gyorsgomb működése
        private void underlineToolStripButton_Click(object sender, EventArgs e)
        {
            underlineToolStripMenuItem_Click(this, e);
        }

        // Fájl megnyitása
        private void OpenFile()
        {
            try
            {
                openFileDialog1.Title = "Megnyitás";
                openFileDialog1.DefaultExt = "rtf";
                openFileDialog1.Filter = "Rich Text|*.rtf|TXT|*.txt|HTML|*.html";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.FileName = string.Empty;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {

                    if (openFileDialog1.FileName == "")
                    {
                        return;
                    }

                    string strExt;
                    strExt = System.IO.Path.GetExtension(openFileDialog1.FileName);
                    strExt = strExt.ToUpper();

                    if (strExt == ".RTF")
                    {
                        richTextBox1.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.RichText);
                    }
                    else
                    {
                        System.IO.StreamReader txtReader;
                        txtReader = new System.IO.StreamReader(openFileDialog1.FileName);
                        richTextBox1.Text = txtReader.ReadToEnd();
                        txtReader.Close();
                        txtReader = null;
                        richTextBox1.SelectionStart = 0;
                        richTextBox1.SelectionLength = 0;
                    }

                    currentFile = openFileDialog1.FileName;
                    richTextBox1.Modified = false;
                    this.Text = "Epsilon: " + currentFile.ToString();
                }
                else
                {
                    MessageBox.Show("A fájlmegnyitás meg lett szakítva a felhasználó által.", "Megszakítva");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Hiba");
            }
        }

        // A Fájl menüre kattintva a betűszín legyen fekete
        private void fileToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            fileToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
        }

        // A Szerkesztés menüre kattintva a betűszín legyen fekete
        private void editToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            editToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
        }

        // A Betűtípus menüre kattintva a betűszín legyen fekete
        private void fontToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            fontToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
        }

        // A Bekezdés menüre kattintva a betűszín legyen fekete
        private void paragraphToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            paragraphToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
        }

        #endregion

        /***** Szövegszerkesztő vége *****/
    }
}
