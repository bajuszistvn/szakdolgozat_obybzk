﻿
namespace Szakdolgozat_OBYBZK
{
    partial class errorMsgForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelErrorMsg = new System.Windows.Forms.Label();
            this.buttonErrorMsgOkay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 41);
            this.panel1.TabIndex = 4;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // labelErrorMsg
            // 
            this.labelErrorMsg.AutoSize = true;
            this.labelErrorMsg.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelErrorMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(193)))), ((int)(((byte)(203)))));
            this.labelErrorMsg.Location = new System.Drawing.Point(67, 40);
            this.labelErrorMsg.Name = "labelErrorMsg";
            this.labelErrorMsg.Size = new System.Drawing.Size(518, 33);
            this.labelErrorMsg.TabIndex = 6;
            this.labelErrorMsg.Text = "Hiba a műveletek végrehajtása során!";
            this.labelErrorMsg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.labelErrorMsg_MouseDown);
            // 
            // buttonErrorMsgOkay
            // 
            this.buttonErrorMsgOkay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.buttonErrorMsgOkay.FlatAppearance.BorderSize = 0;
            this.buttonErrorMsgOkay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonErrorMsgOkay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonErrorMsgOkay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(193)))), ((int)(((byte)(203)))));
            this.buttonErrorMsgOkay.Location = new System.Drawing.Point(265, 110);
            this.buttonErrorMsgOkay.Name = "buttonErrorMsgOkay";
            this.buttonErrorMsgOkay.Size = new System.Drawing.Size(120, 40);
            this.buttonErrorMsgOkay.TabIndex = 7;
            this.buttonErrorMsgOkay.Text = "OK";
            this.buttonErrorMsgOkay.UseVisualStyleBackColor = false;
            this.buttonErrorMsgOkay.Click += new System.EventHandler(this.buttonErrorMsgOkay_Click);
            // 
            // errorMsgForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(57)))), ((int)(((byte)(57)))));
            this.ClientSize = new System.Drawing.Size(652, 254);
            this.Controls.Add(this.buttonErrorMsgOkay);
            this.Controls.Add(this.labelErrorMsg);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "errorMsgForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "errorMsgForm";
            this.Load += new System.EventHandler(this.errorMsgForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.errorMsgForm_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelErrorMsg;
        private System.Windows.Forms.Button buttonErrorMsgOkay;
    }
}