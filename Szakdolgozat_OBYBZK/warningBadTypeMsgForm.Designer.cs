﻿
namespace Szakdolgozat_OBYBZK
{
    partial class warningBadTypeMsgForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonMsgOkay = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(118)))), ((int)(((byte)(43)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 41);
            this.panel1.TabIndex = 5;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // buttonMsgOkay
            // 
            this.buttonMsgOkay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(118)))), ((int)(((byte)(43)))));
            this.buttonMsgOkay.FlatAppearance.BorderSize = 0;
            this.buttonMsgOkay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMsgOkay.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonMsgOkay.Location = new System.Drawing.Point(265, 110);
            this.buttonMsgOkay.Name = "buttonMsgOkay";
            this.buttonMsgOkay.Size = new System.Drawing.Size(120, 40);
            this.buttonMsgOkay.TabIndex = 6;
            this.buttonMsgOkay.Text = "OK";
            this.buttonMsgOkay.UseVisualStyleBackColor = false;
            this.buttonMsgOkay.Click += new System.EventHandler(this.buttonMsgOkay_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(125, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(402, 36);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nem megfelelő kiterjesztés!";
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label1_MouseDown);
            // 
            // warningBadTypeMsgForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(161)))), ((int)(((byte)(61)))));
            this.ClientSize = new System.Drawing.Size(652, 254);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonMsgOkay);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "warningBadTypeMsgForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "warningBadTypeMsgForm";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.warningBadTypeMsgForm_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonMsgOkay;
        private System.Windows.Forms.Label label1;
    }
}